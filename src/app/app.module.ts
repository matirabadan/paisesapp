import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { BuscarComponent } from './components/buscar/buscar.component';
import { PaisesService } from './services/paises.service';

//para hacer peticiones http
import {HttpClientModule} from '@angular/common/http';
import { CardComponent } from './components/card/card.component';
import { PaisComponent } from './components/pais/pais.component';
import { LoadingComponent } from './components/loading/loading.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    BuscarComponent,
    CardComponent,
    PaisComponent,
    LoadingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  //aca van los servicios
  providers: [PaisesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
