import { Component, OnInit } from '@angular/core';
import { PaisesService } from 'src/app/services/paises.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {

  resultado;
  loading = false;

  constructor(private paisService: PaisesService) { }

  ngOnInit() {
  }

  getPais(pais: string) {
    this.loading = true;
    if (pais.length > 0) {
      this.paisService.getPais(pais).subscribe(
        res => {        
          this.resultado = res;
          this.loading = false;
        }
      )
    }else{
      this.loading = false;
    }
  }

}
