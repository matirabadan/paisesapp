import { Component, OnInit } from '@angular/core';
import { PaisesService } from 'src/app/services/paises.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  paises;

  loading = false;

  constructor(private paisesService: PaisesService) {

    //me suscribo al observer
    this.loading = true;
    this.paisesService.getPaises().subscribe(
      res => {
        this.paises = res;
        this.loading = false;
      }    
    );
  }

  ngOnInit() {
  }

}
