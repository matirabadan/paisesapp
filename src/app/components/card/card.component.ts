import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() pais;

  constructor(private router:Router) { }

  ngOnInit() {
  }

  irAPais(){
    let codigo = this.pais.alpha3Code;
    this.router.navigateByUrl(`/pais/${codigo}`);
  }

}
