import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PaisesService } from 'src/app/services/paises.service';

@Component({
  selector: 'app-pais',
  templateUrl: './pais.component.html',
  styleUrls: ['./pais.component.css']
})
export class PaisComponent implements OnInit {

  loading = false;

  pais;

  constructor(private activatedRoute: ActivatedRoute, private paisesService: PaisesService,private router:Router) {
    this.loading = true;
    this.activatedRoute.params.subscribe(
      data => {
        this.paisesService.getPaisCod(data['cod']).subscribe(
          res => {
            this.pais = res;
            this.loading = false;
          }
        )
      }
    );



  }

  ngOnInit() {
  }

  atras(){
    this.router.navigateByUrl('/home');
  }
}
